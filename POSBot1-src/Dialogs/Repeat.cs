using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class Repeat : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await new TransferToAPerson().StartAsync(context);
            /*if(repeat.ToString().ToLower().Contains("cannot understand"))
            {
                context.Call(new TransferToAPerson(), Resumed);
            }*/
        }
    }
}