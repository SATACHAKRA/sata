using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class POSOpenFail : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            //await context.PostAsync(this.message);
            PromptDialog.Confirm(context, ConfirmResume, "Log into a register and confirm when you are ready for the next step. If you do not have a POS ID and password, please have a store manager log in.");
        }

        public async Task ConfirmResume(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm == true)
            {
                await new ConfirmReady().StartAsync(context);
            }
            else
            {
                await context.PostAsync("It seems that you cannot understand what you want");
            }
        }
    }
}