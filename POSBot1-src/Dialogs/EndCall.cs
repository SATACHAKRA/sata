using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class EndCall : IDialog<object>
    {
        public IAwaitable<IMessageActivity> result;

        public EndCall()
        {
        }

        public EndCall(IAwaitable<IMessageActivity> result)
        {
            this.result = result;
        }
        public async Task StartAsync(IDialogContext context)
        {
            if(this.result.ToString().ToLower().Contains("no additional help") || this.result.ToString().ToLower().Contains("quit") || this.result.ToString().ToLower().Contains("stop") || this.result.ToString().ToLower().Contains("not able to complete"))
            {
                await context.PostAsync("Call Disconnected...");
            }
        }
    }
}