using System;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.Luis;
using Microsoft.Bot.Builder.Luis.Models;
using Microsoft.Bot.Connector;
using Microsoft.Bot.Builder.Internals.Fibers;
using System.IO;

namespace Microsoft.Bot.Sample.LuisBot
{
    // For more information about this template visit http://aka.ms/azurebots-csharp-luis
    public class Constants
    {
        public static List<string> WELCOME_STRINGS = new List<string>()
        {
            "Sorry I did not understand your query",
            "Come again",
            "Did not get you. Try again.",
            "I ain't trained for that.",
            "Sorry i don't know how to do that. Try me with something else",
        };
    }
        [Serializable]
    public class FileIO
    {
        public async Task Store(String text)
        {
            /*const string path = "C:\\Users\\Anish\\Desktop\\Conversation.txt";
            using (StreamWriter sw = File.AppendText(path))
            {
                sw.WriteLine(text);
            }*/
        }
    }
    [Serializable]
    public class CloseContact : BasicLuisDialog, IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await new FileIO().Store("Is there any additional help required?");
            await new FileIO().Store("Yes / No");
            PromptDialog.Confirm(context, Confirm, "Is there any additional help required?");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Is there any additional help required?");
            }
        }
        public async Task Confirm(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if (confirm == false)
            {
                await new EndCall().StartAsync(context);
            }
            else if (confirm == true)
            {
                await context.SayAsync(text: "Okay, tell me what can I do for you?. Try asking me things like 'pos error','eid merge','I have some issue',etc.", speak: " ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Speak("Okay, tell me what can I do for you?Try asking me things like 'pos error','eid merge','I have some issue',etc.");
                }
                context.Wait(this.MessageReceived);
            }
        }
    }
    [Serializable]
    public class ConfirmReady : BasicLuisDialog, IDialog<object>
    {
        public List<string> ready = new List<string> { "Let's Go!", "Wait a Minute!" };
        public List<string> proceed = new List<string> { "Yes", "No" };
        public enum Status
        {
            Successful,
            Unsuccessful,
        };
        public async Task StartAsync(IDialogContext context)
        {
            await new FileIO().Store("Ready to dive in!!");
            await new FileIO().Store("Let's Go! / Wait a Minute!");
            PromptDialog.Choice(context, Ready, ready, "Ready to dive in!!");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Ready to dive in!!");
            }
        }

        public async Task Ready(IDialogContext context, IAwaitable<string> result)
        {

            string ready = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            syth.Rate = -2;
            if (ready.ToLower().Contains("go"))
            {
               // await new Gifs().MessageReceivedAsync(context, "https://m.popkey.co/ca7769/vzAZG.gif");
                await context.SayAsync(text: "a. Click on Manager Menu from POS screen", speak: " ");
                syth.Speak("a Click on Manager Menu from P O S screen");
                await context.SayAsync(text: "b. Press Support button", speak: " ");
                syth.Speak("b Press Support button");
                await context.SayAsync(text: "c. Again press Support Button", speak: " ");
                syth.Speak("c Again press Support Button");
                await context.SayAsync(text: "d. Enter Password(Last digit of Year, Last digit of Month, Last Digit of Date)", speak: " ");
                syth.Speak("d Enter Password(Last digit of Year, Last digit of Month, Last Digit of Date)");
                await context.SayAsync(text: "e. Please confirm when ready for next step.", speak: " ");
                syth.Speak("e Please confirm when ready for next step.");
                await new FileIO().Store("Do you want to proceed?");
                await new FileIO().Store("Proceed / Stop here");
                PromptDialog.Choice(context, ResumeConfirm, proceed, "Do you want to proceed?");
                syth.Speak("Do you want to proceed?");
            }
            else if (ready.ToLower().Contains("wait"))
            {
                await context.SayAsync(text: "Let me transfer you to a person...", speak: " ");
                syth.Speak("Let me transfer you to a person");
                await new TransferToAPerson().StartAsync(context);
            }
        }

        public async Task ResumeNotConfirm(IDialogContext context, IAwaitable<object> result)
        {
            await context.SayAsync(text: "Transfer completed.", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Transfer completed.");
            }
        }

        public async Task ResumeConfirm(IDialogContext context, IAwaitable<string> result)
        {

            string progress = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            syth.Rate = -2;
            if (progress.ToLower().Contains("next") || progress.Contains("Yes"))
            {
                //await new Gifs().MessageReceivedAsync(context, "https://meida.giphy.com/meida/pDgHg2Lcju3Ty/giphy.gif");
                await context.SayAsync(text: "a. Press the Cashless Maintenance Button", speak: " ");
                syth.Speak("a Press the Cashless Maintenance Button");
                await context.SayAsync(text: "b. Press Open PED", speak: " ");
                syth.Speak("b Press Open P E D");
                await context.SayAsync(text: "c. Reset the Cashless Device", speak: " ");
                syth.Speak("c Reset the Cashless Device");
                await context.SayAsync(text: "d Select Manager Menu > Special Functions > Reset Cashless device", speak: " ");
                syth.Speak("d. Select Manager Menu, then Special Functions, and then Reset Cashless device");
                await context.SayAsync(text: "e Image will come up: Please be sure there are no cards inserted in the PED before continuing, then Press OK", speak: " ");
                syth.Speak("e. Image will come up: Please be sure there are no cards inserted in the P E D before continuing, then Press OK");
                await context.SayAsync(text: "f. If successful the following message will come up: PED communication is working", speak: " ");
                syth.Speak("f If successful the following message will come up: P E D communication is working");
                await new FileIO().Store("Was the reset successful?");
                await new FileIO().Store("Successful / Unsuccessful");
                //await new Gifs().MessageReceivedAsync(context, "http://gifimage.net/wp-content/uploads/2017/09/are-you-ready-gif-3.gif");
                PromptDialog.Choice(
                          context: context,
                          resume: ResumeStatus,
                          options: (IEnumerable<Status>)Enum.GetValues(typeof(Status)),
                          prompt: "Was the reset successful?",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
                syth.Speak("Was the reset successful?");
            }
            else if (progress.ToLower().Contains("No"))
            {
               // await new Gifs().MessageReceivedAsync(context, "https://meida.giphy.com/meida/rkfvXqtTKxChi/giphy.gif");
                await context.SayAsync(text: "You have cancelled the operation", speak: " ");
                syth.Speak("You have cancelled the operation");
                await context.SayAsync(text: "For your information, try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc", speak: " ");
                syth.Speak("For your information, try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc");
                context.Wait(this.MessageReceived);
            }
        }
        public async Task ResumeStatus(IDialogContext context, IAwaitable<Status> result)
        {
            Status status = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (status == Status.Successful)
            {
                //await new Gifs().MessageReceivedAsync(context, "https://meida.giphy.com/meida/vMnuZGHJfFSTe/giphy.gif");
                await new CreateServiceRequest().StartAsync(context);
            }
            else if (status == Status.Unsuccessful)
            {
                //await new Gifs().MessageReceivedAsync(context, "https://meida2.giphy.com/meida/l1KVaj5UcbHwrBMqI/giphy.gif");
                syth.Rate = -2;
                await context.SayAsync(text: "Performing a PED Rescue via USB is the next step.", speak: " ");
                syth.Speak("Performing a P E D Rescue via USB is the next step.");
                await context.SayAsync(text: "If you would like to view the instructions for the PED rescue, please see KB0113750 on the OTP portal.", speak: " ");
                syth.Speak("If you would like to view the instructions for the P E D rescue, please see KB0113750 on the OTP portal.");
                await context.SayAsync(text: "Please note that a PED rescue may take up to 40 minutes to complete.", speak: " ");
                syth.Speak("Please note that a P E D rescue may take up to 40 minutes to complete.");
                await new TransferToAPerson().StartAsync(context);
            }
        }
        public async Task ResumeService(IDialogContext context, IAwaitable<object> result)
        {
            await context.SayAsync(text: "Service request provided", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("System service provided");
            }
        }
        public async Task ResumeTransfer(IDialogContext context, IAwaitable<object> result)
        {
            await context.SayAsync(text: "Transfer completed", speak: " ");
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.Speak("Transfer completed");
        }
    }
    [Serializable]
    public class CreateLMSTicket : BasicLuisDialog, IDialog<int>
    {
        public List<string> details = new List<string> { "First name", "Last name", "Phone number", "Store number", "Previous eid", "New eid", "Position", "None" };
        public string FirstName = "Not specified";
        public string LastName = "Not specified";
        public string PhoneNumber = "Not specified";
        public string StoreNumber = "Not specified";
        public string Neweid = "Not specified";
        public string Previouseid = "Not specified";
        public string Position = "Not specified";
        public class Constants
        {
            public enum Choice
            {
                Yes,
                No
            };
        }
        public async Task StartAsync(IDialogContext context)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            syth.Rate = -2;
            await context.SayAsync(text: "a. A ticket needs to be created to send to XYZ: LMS.", speak: " ");
            syth.Speak("a A ticket needs to be created to send to XYZ: LMS.");
            await context.SayAsync(text: "b. Please provide the following information on the screen and select submit.", speak: " ");
            syth.Speak("b Please provide the following information on the screen and select submit.");
            await context.SayAsync(text: "i) First Name,  ii) Last Name,  iii) Phone Number,  iv) Store Number,  v) New eid,  vi) Previous eid,  vii)Position", speak: " ");
            syth.Speak("1) First Name,  2) Last Name,  3) Phone Number,  4) Store Number,  5) New e d i,  6) Previous e d i, and 7)Position");
            await new FileIO().Store("Do you want to proceed?");
            await new FileIO().Store("Submit / Cancel");
            PromptDialog.Choice(
                    context: context,
                    resume: Submission,
                    options: (IEnumerable<Constants.Choice>)Enum.GetValues(typeof(Constants.Choice)),
                    prompt: "Do you want to proceed?",
                    retry: "Sorry that option is not available . Please try some other options.",
                    promptStyle: PromptStyle.Auto
                      );
            syth.Speak("Do you want to proceed?");
        }

        public async Task Submission(IDialogContext context, IAwaitable<Constants.Choice> result)
        {
            Constants.Choice choice = await result;
            if (choice == Constants.Choice.Yes)
            {
                await context.SayAsync(text: "Enter your first name:", speak: " ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.Speak("Enter your first name");
                }
                context.Wait(FirstNameReceived);
            }
            else
            {
                await context.SayAsync(text: "You have cancelled the operation. Let's begin again! Try asking me things like 'I have some issue', 'error in pos open', 'eid merge' etc.", speak:" ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Speak("You have cancelled th operation.");
                    syth.Speak("Let's begin again!");
                    syth.Speak("Try asking me things like 'I have some issue', 'error in pos open', 'eid merge' etc.");
                    context.Wait(this.MessageReceived);
                }
            }
        }
        public async Task FirstNameReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.FirstName == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                var FN = await activity;
                this.FirstName = FN.Text;
                //PromptDialog.Confirm(context, FirstNameConfirmed, "Confirm?");
                
                context.SayAsync(text: "Enter the new Last Name", speak: " ");
                syth.Speak("Enter new the Last Name");
                context.Wait(LastNameReceived);
            }
        }
      
        public async Task LastNameReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.LastName == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
               
                var LN = await activity;
                this.LastName = LN.Text;
                
                context.SayAsync(text: "Enter the Phone Number", speak: " ");
                syth.Speak("Enter the Phone Number");
                context.Wait(PhoneNumberReceived);

                //PromptDialog.Confirm(context, LastNameConfirmed, "Confirm?");
                //syth.Speak("Confirm?");
            }
        }
      
        public async Task PhoneNumberReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.PhoneNumber == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                
                var PH = await activity;
                this.PhoneNumber = PH.Text;
                
                context.SayAsync(text: "Enter the Store Number", speak: " ");
                syth.Speak("Enter the Store Number");
                context.Wait(StoreNumberReceived);
            }
        }
      
        public async Task StoreNumberReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.StoreNumber == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
               
                //context.Wait(PeidReceived);
                var SN = await activity;
                this.StoreNumber = SN.Text;
                

                /* PromptDialog.Confirm(context, StoreConfirmed, "Confirm?");
                 syth.Speak("Confirm?");*/
                context.SayAsync(text: "Enter the Previous Eid", speak: " ");
                syth.Speak("Enter the Previous Eid");
                context.Wait(PeidReceived);
            }
        }
       
        public async Task PeidReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.Previouseid == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                
                var PE = await activity;
                this.Previouseid = PE.Text;
                

                /* PromptDialog.Confirm(context, PeidConfirmed, "Confirm?");
                 syth.Speak("Confirm?");*/
                context.SayAsync(text: "Enter the New eid", speak: " ");
                syth.Speak("Enter the New eid");
                context.Wait(NeidReceived);
            }
        }
      
        public async Task NeidReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.Neweid == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
               // context.SayAsync(text: $"The previous NewEid entered by you was {Neweid}", speak: " ");
                //syth.Speak($"The previous NewEid entered by you was {Neweid}");
                var NE = await activity;
                this.Neweid = NE.Text;
                

                /* PromptDialog.Confirm(context, NeidConfirmed, "Confirm?");
                 syth.Speak("Confirm?");*/
                context.SayAsync(text: "Enter the Position", speak: " ");
                syth.Speak("Enter the Position");
                context.Wait(PositionReceived);
            }
        }
     
        public async Task PositionReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            if (this.Position == "Not specified")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                
                var POS = await activity;
                this.Position = POS.Text;

                PromptDialog.Confirm(context, ChangeDetails, "Form fill up completed.Do you want to change any entry?");
                syth.Speak("Form fill up completed.Do you want to change any entry?");
            }
        }
       
        public async Task ChangeDetails(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm==true)
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter which detail you want to change? ", speak: " ");
                syth.Speak("Enter which detail you want to change? ");
               // await context.PostAsync("Enter which detail you want to change? ");
                context.Wait(ChangeDetail1);
            }
            if(confirm==false)
            {
                PromptDialog.Confirm(context, ShowDetails, "Form fill up completed. Do you want to see your details?");
               // syth.Speak("Form fill up completed. Do you want to see your drtails?");
            }

        }
        public async Task ChangeFirstName(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous FirstName entered by you was:{FirstName}", speak: " ");
            syth.Speak($"The previous FirstName entered by you was:{FirstName}");
            var PS = await activity;
            this.FirstName = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangeLastName(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The prevous LastName entered by you was {LastName}", speak: " ");
            syth.Speak($"The prevous LastName entered by you was {LastName}"); var PS = await activity;
            this.LastName = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangePhoneNumber(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous PhoneNumber enterd by you was {PhoneNumber}", speak: " ");
            syth.Speak($"The previous PhoneNumber enterd by you was {PhoneNumber}");
            var PS = await activity;
            this.PhoneNumber = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangeStoreNumber(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous StoreNumber entered by u was {StoreNumber}", speak: " ");
            syth.Speak($"The previous StoreNumber entered by u was {StoreNumber}");
            var PS = await activity;
            this.StoreNumber = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangeNeweid(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous NewEid entered by you was {Neweid}", speak: " ");
            syth.Speak($"The previous NewEid entered by you was {Neweid}");
            var PS = await activity;
            this.Neweid = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangePreviouseid(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous PreviousEid enterd by you was {Previouseid}", speak: " ");
            syth.Speak($"The previous PreviousEid enterd by you was {Previouseid}");
            var PS = await activity;
            this.Previouseid = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangePosition(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            //sythToDefaultAudioDevice();
            syth.SetOutputToDefaultAudioDevice();
            context.SayAsync(text: $"The previous Position enterd by you was {Position}", speak: " ");
            syth.Speak($"The previous Position enterd by you was {Position}");
            var PS = await activity;
            this.Position = PS.Text;
            PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
        }
        public async Task ChangeDetail1(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var POS = await activity;
            if(POS.Text.Contains("FirstName")|| POS.Text.Contains("First Name")|| POS.Text.Contains("First")|| POS.Text.Contains("first Name")|| POS.Text.Contains("first name"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new FirstName: ", speak: " ");
                syth.Speak("Enter the new FirstName: ");
                //await context.PostAsync("Enter the FirstName: ");
                context.Wait(ChangeFirstName);
                //PromptDialog.Confirm(context, ChangeDetails, "Do you want tochange any other detail?");
            }
          else if (POS.Text.Contains("LastName")|| POS.Text.Contains("Last Name")|| POS.Text.Contains("Last")|| POS.Text.Contains("last Name"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new LastName: ", speak: " ");
                syth.Speak("Enter the new LastName: ");
               // await context.PostAsync("Enter the LastName: ");
                 context.Wait(ChangeLastName);
                
            }
           else if (POS.Text.Contains("PhoneNumber")|| POS.Text.Contains("Phone Number")|| POS.Text.Contains("phone number"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new PhonNumber: ", speak: " ");
                syth.Speak("Enter the new PhoneNumber: ");
                //await context.PostAsync("Enter the Phone: ");
                context.Wait(ChangePhoneNumber);
                
            }
           else if (POS.Text.Contains("StoreNumber")| POS.Text.Contains("Store Number")|| POS.Text.Contains("store number"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new StoreNumber: ", speak: " ");
                syth.Speak("Enter the new StoreNumber: ");
                //await context.PostAsync("Enter the StoreNumber: ");
                 context.Wait(ChangeStoreNumber);
                
            }
           else if (POS.Text.Contains("Neweid")|| POS.Text.Contains("Neweid")|| POS.Text.Contains("New employee id")|| POS.Text.Contains("new eid")|| POS.Text.Contains("new employe id"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new NewEid: ", speak: " ");
                syth.Speak("Enter the new NewEid: ");
                //await context.PostAsync("Enter the Neweid: ");
                context.Wait(ChangeNeweid);
               
            }
           else if (POS.Text.Contains("Previouseid")|| POS.Text.Contains("previouseid") || POS.Text.Contains("Previous employee id") || POS.Text.Contains("previous eid") || POS.Text.Contains("previous employe id"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new PreviousEid: ", speak: " ");
                syth.Speak("Enter the new PreviousEid: ");
              //  await context.PostAsync("Enter the previouseid: ");
                context.Wait(ChangePreviouseid);
            }
          else  if (POS.Text.Contains("Position")|| POS.Text.Contains("position"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Enter the new Position: ", speak: " ");
                syth.Speak("Enter the new Position: ");
                //await context.PostAsync("Enter the Position: ");
                context.Wait(ChangePosition);
               
            }
            else
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                //sythToDefaultAudioDevice();
                syth.SetOutputToDefaultAudioDevice();
                context.SayAsync(text: "Sorry I do not know what you mean by that. Try again  ", speak: " ");
                syth.Speak("Sorry I do not know what you mean by that. Try again  ");
                context.Wait(ChangeDetail1);
            }
        }
       /* public async Task ReceivePosition(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var POS = await activity;
            this.Position = POS.Text;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            await new FileIO().Store("Confirm?");
            await new FileIO().Store("Yes / No");
            PromptDialog.Confirm(context, PositionConfirmed, "Confirm?");
            syth.Speak("Confirm?");
        }*/
        public async Task ShowDetails(IDialogContext context, IAwaitable<bool> result)
        {
            bool show = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (show == true)
            {
                await context.SayAsync(text: $"FirstName: {this.FirstName}", speak: " ");
                syth.Speak($"First Name: {this.FirstName}");
                await context.SayAsync(text: $"LastName: {this.LastName}", speak: " ");
                syth.Speak($"Last Name: {this.LastName}");
                await context.SayAsync(text: $"PhoneNumber: {this.PhoneNumber}", speak: " ");
                syth.Speak($"Phone Number: {this.PhoneNumber}");
                await context.SayAsync(text: $"StoreNumber: {this.StoreNumber}", speak: " ");
                syth.Speak($"Store Number: {this.StoreNumber}");
                await context.SayAsync(text: $"Previouseid: {this.Previouseid}", speak: " ");
                syth.Speak($"Previous e d i: {this.Previouseid}");
                await context.SayAsync(text: $"Neweid: {this.Neweid}", speak: " ");
                syth.Speak($"New e d i: {this.Neweid}");
                await context.SayAsync(text: $"Position: {this.Position}", speak: " ");
                syth.Speak($"Position: {this.Position}");
            }
            PromptDialog.Choice(
                        context: context,
                        resume: SubmissionForm,
                        options: (IEnumerable<Constants.Choice>)Enum.GetValues(typeof(Constants.Choice)),
                        prompt: "Do you want to proceed?",
                        retry: "Sorry that option is not available . Please try some other options.",
                        promptStyle: PromptStyle.Auto
                        );
            syth.Speak("Do you want to proceed?");

        }
        /*public async Task FormConfirm(IDialogContext context, IAwaitable<string> result)
        {
            string details = await result;
            if (details.ToLower() == "first name")
            {
                this.FirstName = "Not specified";
                await context.SayAsync(text: "Enter your first name:", speak: " ");
                context.Wait(FirstNameReceived);
            }
            else if (details.ToLower() == "last name")
            {
                this.LastName = "Not specified";
                await context.SayAsync(text: "Enter your last name:", speak: " ");
                context.Wait(LastNameReceived);
            }
            else if (details.ToLower() == "phone number")
            {
                this.PhoneNumber = "Not specified";
                await context.SayAsync(text: "Enter your phone number:", speak: " ");
                context.Wait(PhoneNumberReceived);
            }
            else if (details.ToLower() == "store number")
            {
                this.StoreNumber = "Not specified";
                await context.SayAsync(text: "Enter your store number:", speak: " ");
                context.Wait(StoreNumberReceived);
            }
            else if (details.ToLower() == "previous eid")
            {
                this.Previouseid = "Not specified";
                await context.SayAsync(text: "Enter your previous eid:", speak: " ");
                context.Wait(PeidReceived);
            }
            else if (details.ToLower() == "new eid")
            {
                this.Neweid = "Not specified";
                await context.SayAsync(text: "Enter your new eid:", speak: " ");
                context.Wait(NeidReceived);
            }
            else if (details.ToLower() == "position")
            {
                this.Position = "Not specified";
                await context.SayAsync(text: "Enter your position:", speak: " ");
                context.Wait(PositionReceived);
            }
            else if (details.ToLower() == "none")
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
               
            }
        }*/
        public async Task SubmissionForm(IDialogContext context, IAwaitable<Constants.Choice> result)
        {
            Constants.Choice choice = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (choice == Constants.Choice.Yes)
            {
                var ticketNumber = new Random().Next(0, 20000);
                await context.SayAsync(text: "Your message was registered.", speak: " ");
                syth.Speak("Your message was registered");
                await context.SayAsync(text: "Once we resolve it, we will get back to you.", speak: " ");
                syth.Speak("Once we resolve it, we will get back to you");
                await context.SayAsync(text: $"Your ticket number is {ticketNumber}", speak: " ");
                syth.Speak($"Your ticket number is {ticketNumber}");
                //await new FileIO().Store("Do you want to merge another employee?");
                //await new FileIO().Store("Yes / No");
                PromptDialog.Confirm(context, Confirmed, "Do you want to merge another employee?");
                syth.Speak("Do you want to merge another employee?");
            }
            else
            {
                await context.SayAsync(text: "You have cancelled the operation", speak: " ");
                syth.Speak("You have cancelled the operation");
                context.Wait(this.MessageReceived);
            }
        }

        public async Task Confirmed(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if (confirm == true)
            {
                await new CreateLMSTicket().StartAsync(context);
            }
            else
            {
                await context.SayAsync(text: "LMSTicket is provided", speak: " ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Speak("L M S Ticket is provided");
                }
                await new CloseContact().StartAsync(context);
            }
        }
        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var ticketNumber = new Random().Next(0, 20000);
            await context.SayAsync(text: $"Your message '{message.Text}' was registered. Once we resolve it, we will get back to you.", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak($"Your message '{message.Text}' was registered. Once we resolve it, we will get back to you.");
            }
            context.Done(ticketNumber);
        }
    }
    [Serializable]
    public class CreateServiceRequest : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            syth.Rate = -2;
            //await context.SayAsync(text: "Bot provides service now ticket in service now.", speak: " ");
            //syth.Speak("Bot provides service now ticket in service now");
            var incidentNumber = new Random().Next(0, 20000);
            await context.SayAsync(text: $" An incident has been created for you. Your Service Now incident Number is {incidentNumber}.", speak: " ");
            syth.Speak($" An incident has been created for you. Your Service Now incident Number is {incidentNumber}.");
            await new CloseContact().StartAsync(context);
        }
        public async Task Info(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var incidentNumber = new Random().Next(0, 20000);
            await context.SayAsync(text: $"Bot service provides now ticket # to the user. An incident has been created for you. Your Service Now incident Number is {incidentNumber}.", speak: " ");
        }
    }
    [Serializable]
    public class EIDMerge : BasicLuisDialog, IDialog<object>
    {
        public List<string> confirm = new List<string> { "Yes, it is completed", "No, it is incomplete" };
        public List<string> assistance = new List<string> { "Person Merge Assistance", "Person Merge Information", "Global Account Manager" };
        public List<string> level = new List<string> { "Staff level", "Store level" };
        public List<string> support = new List<string> { "I want to see presentation", "I want to contact" };
        public class Constants
        {
            public enum Level
            {
                Staff,
                Store
            };
            public enum Support
            {
                Presentation,
                Contact
            };
        }
        public async Task StartAsync(IDialogContext context)
        {
            await new FileIO().Store("Has a person merge been completed?");
            await new FileIO().Store("Yes, it is complete / No, it is incomplete");
            PromptDialog.Confirm(context, MergeConfirm, "Has a person merge been completed?");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Has a person merge been completed?");
            }
        }
        public async Task MergeConfirm(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if (confirm==true
                )
            {
                await new CreateLMSTicket().StartAsync(context);
            }
            else
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                syth.Rate = -2;
                await context.SayAsync(text: "A Person Merge needs to be completed.", speak: " ");
                syth.Speak("A person merge needs to be completed.");
                await context.SayAsync(text: "Do you need assistance with the Person Merge Process, information about what Person Merge is, or training and instructions for Global Account Manager?", speak: " ");
                syth.Speak("Do you need assistance with the Person Merge Process, information about what Person Merge is, or training and instructions for Global Account Manager?");
                PromptDialog.Choice(context, Assistance, assistance, "Please select one option");
                syth.Speak("Please select one option");
            }
        }
        public async Task ResumeTicket(IDialogContext context, IAwaitable<int> result)
        {
            var ticketNumber = await result;
            await context.SayAsync(text: $"Thanks for contacting our support team. Your ticket number is {ticketNumber}.", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak($"Thanks for contacting our support team. Your ticket number is {ticketNumber}.");
            }
        }
        public async Task Assistance(IDialogContext context, IAwaitable<string> result)
        {
            string assistance = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (assistance.ToLower().Contains("person merge assistance"))
            {
                await new FileIO().Store("Is the person merge needed for a store level employee or staff level employee?");
                await new FileIO().Store("Store level / Staff level");
                PromptDialog.Choice(context, Level, level, "Is the Person Merge needed for a store level employee or staff level employee?");
                syth.Speak("Is the Person Merge needed for a store level employee or staff level employee?");
            }
            else if (assistance.ToLower().Contains("person merge information"))
            {
                await context.SayAsync(text: "A Person Merge is required whenever an employee who has training associated with a prior eid acquires a new eid.  The Person Merge process transfers all XYZ’s learning transcripts to the new eid to ensure the employee's records include all past training.  This allows employees to have one training transcript with their entire XYZ’s training history.", speak: " ");
                syth.Speak("A Person Merge is required whenever an employee who has training associated with a prior eid acquires a new eid.  The Person Merge process transfers all XYZ’s learning transcripts to the new eid to ensure the employee's records include all past training.  This allows employees to have one training transcript with their entire XYZ’s training history.");
                await context.SayAsync(text: "An employee may require a Person Merge in a number of situations, including the following: • The employee has transferred to a new organization and a new eid was created for them • The employee left the company and came back and new eid was created for them • eids can be disabled manually when an employee leaves the company • eids are disabled automatically after 6 months of inactivity • The employee was promoted from crew trainer to manager • A store was acquired by a new O/ O and the new O/ O created new eids for managers", speak: " ");
                syth.Speak("An employee may require a Person Merge in a number of situations, including the following: • The employee has transferred to a new organization and a new eid was created for them • The employee left the company and came back and new eid was created for them • eids can be disabled manually when an employee leaves the company • eids are disabled automatically after 6 months of inactivity • The employee was promoted from crew trainer to manager • A store was acquired by a new O/ O and the new O/ O created new eids for managers");
                await new FileIO().Store("Do you need assistance with a Person Merge now?");
                await new FileIO().Store("Yes / No");
                PromptDialog.Confirm(context, InfoResume, "Do you need assistance with a Person Merge now?");
                syth.Speak("Do you need assistance with a Person Merge now?");
            }
            else if (assistance.ToLower().Contains("global account manager"))
            {
                await context.SayAsync(text: "For training, instructions, and additional information on Global Account Manager, please see the Global Account Manager Lunch and Learn presentation on AccessXYZ, or contact GAM Support at GlobalAMSupport@us.xyz.com.", speak: " ");
                syth.Speak("For training, instructions, and additional information on Global Account Manager, please see the Global Account Manager Launch and Learn presentation on AccessXYZ, or contact GAM Support at GlobalAMSupport@us.xyz.com.");
                await new FileIO().Store("Select one option");
                await new FileIO().Store("I want to see presentation / I want to contact");
                PromptDialog.Choice(context, SupportOption, support, "Select one option");
                syth.Speak("Select one option.");
            }
        }
        public async Task SupportOption(IDialogContext context, IAwaitable<string> result)
        {
            string support = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (support.ToLower().Contains("presentation"))
            {
                await context.SayAsync(text: "https://www.canva.com", speak: " ");
                syth.Speak("Press this link to see the presentation");
            }
            else
            {
                await context.SayAsync(text: "https://www.canva.com", speak: " ");
                syth.Speak("Press this link to contact us");
            }
            context.Wait(this.MessageReceived);
        }
        public async Task InfoResume(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if (confirm == true)
            {
                await context.SayAsync(text: "Go to Person Merge", speak: " ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Speak("Go to person merge");
                }
            }
            else
            {
                await context.SayAsync(text: "So you don't need assistance", speak: " ");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Speak("So you do not need assistance");
                }
            }
            await context.SayAsync(text: "You can now ask new questions. Try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("You can now ask new questions. Try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc");
            }
            context.Wait(this.MessageReceived);
        }
        public async Task Level(IDialogContext context, IAwaitable<string> result)
        {
            string level = await result;
            if (level.ToLower().Contains("staff"))
            {
                await context.SayAsync("Contact the xyz Corporate help desk through Service Café or at 630-623-5000 to assist with the eid merge.");
                using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                {
                    syth.SetOutputToDefaultAudioDevice();
                    syth.Rate = -2;
                    syth.Speak("Contact the xyz Corporate help desk through Service Café or at 630-623-5000 to assist with the eid merge.");
                }
                context.Wait(this.MessageReceived);
            }
            else if (level.ToLower().Contains("store"))
            {
                await new CreateLMSTicket().StartAsync(context);
            }
        }

        public async Task TicketResume(IDialogContext context, IAwaitable<int> result)
        {
            var TicketNumber = await result;
            //System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            await context.SayAsync(text: $"Your ticket number is {TicketNumber}", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak($"Your ticket number is {TicketNumber}");
            }
        }
    }
    [Serializable]
    public class EndCall : BasicLuisDialog, IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
           // await new Gifs().MessageReceivedAsync(context, "http://animated.name/uploads/posts/2016-08/1471431606_816.gif");
            await context.SayAsync(text: "Call Disconnected...", speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Call disconnected...");
            }
            context.Wait(this.MessageReceived);
        }
    }
    [Serializable]
    public class POSOpenFail : BasicLuisDialog, IDialog<object>
    {
        public List<string> confirm = new List<string> { "Yeah!", "Nope!" };
        public async Task StartAsync(IDialogContext context)
        {
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            syth.Rate = -2;
            await context.SayAsync(text: "Log into a register and confirm when you are ready for the next step.", speak: " ");
            syth.Speak("Log into a register and confirm when you are ready for the next step.");
            await context.SayAsync(text: "If you do not have a POS ID and password, please have a store manager log in.", speak: " ");
            syth.Speak("If you do not have a P O S ID and password, please have a store manager log in.");
            await new FileIO().Store("Do you confirm?");
            await new FileIO().Store("Yeah / Nope");
            PromptDialog.Choice(context, ConfirmResume, confirm, "Do you confirm?");
            syth.Speak("Do you confirm?");
        }

        public async Task ConfirmResume(IDialogContext context, IAwaitable<string> result)
        {
            string confirm = await result;
            if (confirm.ToLower().Contains("yeah") || confirm.ToLower().Contains("yes") || confirm.ToLower().Contains("yep"))
            {
                await new ConfirmReady().StartAsync(context);
            }
            else if (confirm.ToLower().Contains("nope") || confirm.ToLower().Contains("no"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                await context.SayAsync(text: "It seems that you cannot understand what you want", speak: " ");
                syth.Speak("It seems that you cannot understand what you want");
                await context.SayAsync(text: "Let me transfer you to a person for your better understanding", speak: " ");
                syth.Speak("Let me transfer you to a person for your better understanding");
                await new TransferToAPerson().StartAsync(context);
            }
        }
    }
    [Serializable]
    public class Repeat : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await new TransferToAPerson().StartAsync(context);
        }
    }
    [Serializable]
    public class TransferToAPerson : BasicLuisDialog, IDialog<object>
    {
        public List<string> transfer = new List<string> { "Yes, transfer me", "No, maybe later" };
        public async Task StartAsync(IDialogContext context)
        {
            await new FileIO().Store("Do you want to be transferred now?");
            await new FileIO().Store("Yes, transfer me / No, maybe later");
            PromptDialog.Choice(context, TransferConfirmed, transfer, "Do you want to be transferred now?");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Do you want to be transferred now?");
            }
        }
        public async Task TransferConfirmed(IDialogContext context, IAwaitable<string> result)
        {
            string confirm = await result;
            System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
            syth.SetOutputToDefaultAudioDevice();
            if (confirm.ToLower().Contains("yes") || confirm.ToLower().Contains("transfer"))
            {
                await context.SayAsync(text: "You are transferred to a customer service representative...", speak: " ");
                syth.Speak("You are transferred to a customer service representative");
                await context.SayAsync(text: "You can now ask new questions. Try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc", speak: " ");
                syth.Speak("You can now ask new questions. Try asking me things like 'I have some issue', 'Error in p o s open', 'e d i merge' etc");
                context.Wait(this.MessageReceived);
            }
            else if (confirm.ToLower().Contains("no") || confirm.ToLower().Contains("do not") || confirm.ToLower().Contains("don't") || confirm.ToLower().Contains("later"))
            {
                await new CloseContact().StartAsync(context);
            }
        }
    }
    [Serializable]
    public class BasicLuisDialog : LuisDialog<object>
    {
        public BasicLuisDialog() : base(new LuisService(new LuisModelAttribute(
            ConfigurationManager.AppSettings["LuisAppId"],
            ConfigurationManager.AppSettings["LuisAPIKey"],
            domain: ConfigurationManager.AppSettings["LuisAPIHostName"])))
        {
        }
        public List<string> issue = new List<string> { "Point of Sale (POS)", "EID Merge(Employee ID)" };
        public List<string> confirm = new List<string> { "Yes I did", "No, I did not" };
        public enum MyIssue
        {
            POS,
            eid
        };
        public string myissue;
        public MyIssue myIssue;
        public string GetRandomString(List<string> stringList)
        {
            Random random = new Random();
            int idx = random.Next(0, 1000) % stringList.Count;
            return stringList[idx];
        }
        [LuisIntent("None")]
        public async Task NoneIntent(IDialogContext context, LuisResult result)
        {
            /* await context.SayAsync(text: "I cannot understand your query...", speak: " ");
             using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
             {
                 syth.SetOutputToDefaultAudioDevice();
                 syth.Speak("I cannot understand your query");
             }*/
            string str = GetRandomString(Constants.WELCOME_STRINGS);
            await context.SayAsync(text:str,speak: " ");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak(str);
            }
            //context.Wait(this.MessageReceived);
            context.Wait(this.MessageReceived);
        }
        [LuisIntent("Issue")]
        public async Task IssueIntent(IDialogContext context, LuisResult result)
        {
            await new FileIO().Store("Please select one option");
            await new FileIO().Store("Point of Sale (POS) / EID Merge");
            PromptDialog.Choice(context, Issue, issue, "Please select one option");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                syth.Speak("Please select one option");
                syth.Speak("The options are: point of sale and E I D Merge");
            }
        }
        public async Task Issue(IDialogContext context, IAwaitable<string> result)
        {
            this.myissue = await result;
            await new FileIO().Store($"Are you calling for {this.myissue}??");
            await new FileIO().Store("Yes, I did / No, I did not");
            PromptDialog.Choice(context, IssueConfirmed, confirm, $"Are you calling for {this.myissue}??");
            using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
            {
                syth.SetOutputToDefaultAudioDevice();
                if (myissue.ToLower().Contains("point of sale"))
                {
                    syth.Speak("Are you calling for point of sale?");
                }
                else if (myissue.Contains("Merge")|| this.myissue.Contains("EID Merge") || this.myissue.Contains("EID") ||  this.myissue.Contains("E I D") || this.myissue.Contains("id") || this.myissue.Contains("ID"))
                {
                    syth.Speak("Are you calling for EID Merge");
                }
            }
        }
        public async Task IssueConfirmed(IDialogContext context, IAwaitable<string> result)
        {
            string confirm = await result;
            if (confirm.ToLower().Contains("yes") || confirm.ToLower().Contains("yeah"))
            {
                if (this.myissue.ToLower().Contains("point of sale"))
                {
                    await new POSOpenFail().StartAsync(context);
                }
                else if (this.myissue.Contains("Employee id")||this.myissue.Contains("Employee")|| this.myissue.Contains("EID")|| this.myissue.Contains("id")|| this.myissue.Contains("ID")||myissue.ToLower().Contains("Merge") || this.myissue.Contains("EID Merge") || this.myissue.Contains("EID") || this.myissue.Contains("E I D") || this.myissue.Contains("id") || this.myissue.Contains("ID"))
                {
                    await new EIDMerge().StartAsync(context);
                }
            }
            else if (confirm.ToLower().Contains("no"))
            {
                System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer();
                syth.SetOutputToDefaultAudioDevice();
                await context.SayAsync(text: "Oops! Let's begin again. Try asking me things like 'I have some issue', 'Error in pos open', 'eid merge' etc", speak: " ");
                syth.Speak("Oops! Let's begin again. Try asking me things like 'I have some issue', 'Error in pos open', 'eid merge' etc");
                context.Wait(this.MessageReceived);
            }
        }
        [LuisIntent("ReportOpenFailError")]
        public async Task ReportOpenFailErrorIntent(IDialogContext context, LuisResult result)
        {
            await new POSOpenFail().StartAsync(context);
        }
        [LuisIntent("EIDMerge")]
        public async Task EIDMergeIntent(IDialogContext context, IAwaitable<IMessageActivity> message, LuisResult result)
        {
            await new EIDMerge().StartAsync(context);
        }
    }
}