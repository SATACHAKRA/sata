using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class ConfirmReady : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            PromptDialog.Confirm(context, Ready, "Ready?");
        }
        public enum Progress
        {
            Next,
            Cancel
        };
        public enum Status
        {
            Successful,
            Unsuccessful
        };

        public async Task Ready(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm==true)
            {
                PromptDialog.Choice(
                context: context,
                resume: ResumeConfirm,
                options: (IEnumerable<Progress>)Enum.GetValues(typeof(Progress)),
                          prompt: "a. Click on Manager Menu from POS screen,   b. Press Support button,   c. Again press Support Button,   d. Enter Password(Last digit of Year, Last digit of Month, Last Digit of Date),   e. Please confirm when ready for next step.",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
            }
            else
            {
                await context.PostAsync("Let me transfer you to a person...");
                await new TransferToAPerson().StartAsync(context);
                //context.Call(new TransferToAPerson(), ResumeNotConfirm);
            }
        }

        public async Task ResumeNotConfirm(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("Transfer completed.");
        }

        public async Task ResumeConfirm(IDialogContext context, IAwaitable<Progress> result)
        {
            Progress progress = await result;
            if(progress==Progress.Next)
            {
                PromptDialog.Choice(
                context: context,
                resume: ResumeStatus,
                options: (IEnumerable<Status>)Enum.GetValues(typeof(Status)),
                prompt: "a. Press the Cashless Maintenance Button,   b. Press Open PED,   c. Reset the Cashless Device,   d. Select Manager Menu > Special Functions > Reset Cashless device,   e. Image will come up: Please be sure there are no cards inserted in the PED before continuing > Press OK,   f. If successful the following message will come up: PED communication is working,   g. Was the reset successful?",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
            }
        }
        public async Task ResumeStatus(IDialogContext context, IAwaitable<Status> result)
        {
            Status status = await result;
            if (status == Status.Successful)
            {
                await new CreateServiceRequest().StartAsync(context);
                //context.Call(new CreateServiceRequest(), ResumeService);
            }
            else
            {
                await context.PostAsync("Performing a PED Rescue via USB is the next step.  If you would like to view the instructions for the PED rescue, please see KB0113750 on the OTP portal. Please note that a PED rescue may take up to 40 minutes to complete.");
                //context.Call(new TransferToAPerson(), ResumeTransfer);
                await new TransferToAPerson().StartAsync(context);
            }
        }
        public async Task ResumeService(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("Service request provided");
        }
        public async Task ResumeTransfer(IDialogContext context, IAwaitable<object> result)
        {
            await context.PostAsync("Transfer completed");
        }
    }
}