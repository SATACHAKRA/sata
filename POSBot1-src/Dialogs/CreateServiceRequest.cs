using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class CreateServiceRequest : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Bot provides service now ticket in service now.");
            var incidentNumber = new Random().Next(0, 20000);
            await context.PostAsync($"Bot service provides now ticket # to the user. An incident has been created for you. Your Service Now incident Number is {incidentNumber}.");
        }
        public async Task Info(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var incidentNumber = new Random().Next(0, 20000);
            await context.PostAsync($"Bot service provides now ticket # to the user. An incident has been created for you. Your Service Now incident Number is {incidentNumber}.");
        }
    }
}