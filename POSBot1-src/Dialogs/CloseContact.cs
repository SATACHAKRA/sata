using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
    [Serializable]
	public class CloseContact : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            PromptDialog.Confirm(context, Confirm, "Is there any additional help required?");
        }
        public async Task Confirm(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if (confirm == false)
            {
                await new EndCall().StartAsync(context);
            }
            else if(confirm == true)
            {
                await context.PostAsync("Okay, tell me what can I do for you?");
            }
        }
    }
}