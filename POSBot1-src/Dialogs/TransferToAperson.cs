using Microsoft.Bot.Builder.Dialogs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class TransferToAPerson : IDialog<object>
    {
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("Transfer is getting processed...");
            PromptDialog.Confirm(context, TransferConfirmed, "Do you want to be transferred now?");
        }
        public async Task TransferConfirmed(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm == true)
            {
                await context.PostAsync("You are being transferred to a customer service representative...");
            }
            else
            {
                await new Repeat().StartAsync(context);
            }
        }
    }
}