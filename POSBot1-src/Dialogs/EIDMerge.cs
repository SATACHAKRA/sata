using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class EIDMerge : IDialog<object>
    {
        public class Constants
        {
            public enum Assistance
            {
                PersonMergeAssistance,
                PersonMergeInformation,
                GlobalAccountManager
            };
            public enum Level
            {
                Staff,
                Store
            };
        }
        public async Task StartAsync(IDialogContext context)
        {
            PromptDialog.Confirm(context, MergeConfirm, "Has a person merge been completed?");
        }
        public async Task MergeConfirm(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm == true)
            {
                await new CreateLMSTicket().StartAsync(context);
            }
            else
            {
                PromptDialog.Choice(
                context: context,
                resume: Assistance,
                options: (IEnumerable<Constants.Assistance>)Enum.GetValues(typeof(Constants.Assistance)),
                          prompt: "A Person Merge needs to be completed. Do you need assistance with the Person Merge Process, information about what Person Merge is, or training and instructions for Global Account Manager?",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
            }
        }
        public async Task ResumeTicket(IDialogContext context, IAwaitable<int> result)
        {
            var ticketNumber = await result;
            await context.PostAsync($"Thanks for contacting our support team. Your ticket number is {ticketNumber}.");
        }
        public async Task Assistance(IDialogContext context, IAwaitable<Constants.Assistance> result)
        {
            Constants.Assistance assistance = await result;
            if(assistance==Constants.Assistance.PersonMergeAssistance)
            {
                PromptDialog.Choice(
                context: context,
                resume: Level,
                options: (IEnumerable<Constants.Level>)Enum.GetValues(typeof(Constants.Level)),
                          prompt: "Is the Person Merge needed for a store employee or staff level employee?",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
            }
            else if(assistance==Constants.Assistance.PersonMergeInformation)
            {
                await context.PostAsync("A Person Merge is required whenever an employee who has training associated with a prior EID acquires a new EID.  The Person Merge process transfers all McDonald’s learning transcripts to the new EID to ensure the employee's records include all past training.  This allows employees to have one training transcript with their entire McDonald’s training history.");
                await context.PostAsync("An employee may require a Person Merge in a number of situations, including the following: • The employee has transferred to a new organization and a new EID was created for them • The employee left the company and came back and new EID was created for them • EIDs can be disabled manually when an employee leaves the company • EIDs are disabled automatically after 6 months of inactivity • The employee was promoted from crew trainer to manager • A store was acquired by a new O/ O and the new O/ O created new EIDs for managers");
                PromptDialog.Confirm(context, InfoResume, "Do you need assistance with a Person Merge now?");
            }
            else if(assistance==Constants.Assistance.GlobalAccountManager)
            {
                var resultMessage = context.MakeMessage();
                resultMessage.AttachmentLayout = AttachmentLayoutTypes.Carousel;
                resultMessage.Attachments = new List<Attachment>();
                ThumbnailCard thumbnailCard = new ThumbnailCard()
                {
                    Buttons = new List<CardAction>()
                    {
                        new CardAction()
                        {
                            Title = "Presentation Link",
                            Type = ActionTypes.OpenUrl,
                            Value = "https://www.impgroup.org/uploads/papers/69.pdf",
                        }
                    }
                };
                resultMessage.Attachments.Add(thumbnailCard.ToAttachment());
                await context.PostAsync(resultMessage);
            }
        }
        public async Task InfoResume(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            if(confirm == true)
            {
                await context.PostAsync("Go to Person Merge");
            }
            else
            {
                await context.PostAsync("So you dont need assistance");
            }
        }
        public async Task Level(IDialogContext context, IAwaitable<Constants.Level> result)
        {
            Constants.Level level = await result;
            if(level==Constants.Level.Staff)
            {
                await context.PostAsync("Contact the McDonald’s Corporate help desk through Service Café or at 630-623-5000 to assist with the EID merge.");
            }
            else
            {
                await new CreateLMSTicket().StartAsync(context);
                //await context.Forward(new CreateLMSTicket(), TicketResume, result, CancellationToken.None);
            }
        }

        public async Task TicketResume(IDialogContext context, IAwaitable<int> result)
        {
            var TicketNumber = await result;
            await context.PostAsync($"Your ticket number is {TicketNumber}");
        }
    }
}