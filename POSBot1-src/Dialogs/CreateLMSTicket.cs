using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Builder.FormFlow;
using Microsoft.Bot.Connector;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
namespace Microsoft.Bot.Sample.LuisBot
{
	[Serializable]
    public class CreateLMSTicket : IDialog<int>
    {
        public string FirstName;
        public string LastName;
        public string PhoneNumber;
        public string StoreNumber;
        public string NewEID;
        public string PreviousEID;
        public string Position;
        public class Constants
        {
            public enum Choice
            {
                Submit,
                Cancel
            };
        }
        public async Task StartAsync(IDialogContext context)
        {
            await context.PostAsync("a.	A ticket needs to be created to send to MCD: LMS.   b.Please provide the following information on the screen and select submit.");
            await context.PostAsync("i) First Name,  ii) Last Name,  iii) Phone Number,  iv) Store Number,  v) New EID,  vi) Previous EID,  vii)Position");
            PromptDialog.Choice(
            context: context,
            resume: Submission,
            options: (IEnumerable<Constants.Choice>)Enum.GetValues(typeof(Constants.Choice)),
                      prompt: "Proceed?",
                      retry: "Sorry that option is not available . Please try some other options.",
                      promptStyle: PromptStyle.Auto
                      );
        }

        public async Task Submission(IDialogContext context, IAwaitable<Constants.Choice> result)
        {
            Constants.Choice choice = await result;
            if(choice==Constants.Choice.Submit)
            {
                await context.PostAsync("Enter your first name:");
                context.Wait(FirstNameReceived);
            }
        }
        public async Task FirstNameReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var FN = await activity;
            this.FirstName = FN.Text;
            await context.PostAsync("Enter your last name:");
            context.Wait(LastNameReceived);
        }
        public async Task LastNameReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var LN = await activity;
            this.LastName = LN.Text;
            await context.PostAsync("Enter your phone number:");
            context.Wait(PhoneNumberReceived);
        }
        public async Task PhoneNumberReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var PH = await activity;
            this.PhoneNumber = PH.Text;
            await context.PostAsync("Enter your store number:");
            context.Wait(StoreNumberReceived);
        }
        public async Task StoreNumberReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var SN = await activity;
            this.StoreNumber = SN.Text;
            await context.PostAsync("Enter your previous EID:");
            context.Wait(PEIDReceived);
        }
        public async Task PEIDReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var PE = await activity;
            this.PreviousEID = PE.Text;
            await context.PostAsync("Enter your new EID:");
            context.Wait(NEIDReceived);
        }
        public async Task NEIDReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var NE = await activity;
            this.NewEID = NE.Text;
            await context.PostAsync("Enter your position:");
            context.Wait(PositionReceived);
        }
        public async Task PositionReceived(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var POS = await activity;
            this.Position = POS.Text;
            await context.PostAsync("Form completed. Do you want to see your details?");
            context.Wait(ShowDetails);
        }
        public async Task ShowDetails(IDialogContext context, IAwaitable<IMessageActivity> activity)
        {
            var show = await activity;
            if (show.Text.ToLower().Contains("yes"))
            {
                await context.PostAsync($"FirstName: {this.FirstName}, LastName: {this.LastName}, PhoneNumber: {this.PhoneNumber}, StoreNumber: {this.StoreNumber}, PreviousEID: {this.PreviousEID}, NewEID: {this.NewEID}, Position: {this.Position}.");
            }
            PromptDialog.Choice(
                context: context,
                resume: SubmissionForm,
                options: (IEnumerable<Constants.Choice>)Enum.GetValues(typeof(Constants.Choice)),
                          prompt: "Proceed?",
                          retry: "Sorry that option is not available . Please try some other options.",
                          promptStyle: PromptStyle.Auto
                          );
        }
        public async Task SubmissionForm(IDialogContext context, IAwaitable<Constants.Choice> result)
        {
            Constants.Choice choice = await result;
            if(choice==Constants.Choice.Submit)
            {
                PromptDialog.Confirm(context, Confirmed, "Do you want to merge another employee?");
            }
        }

        public async Task Confirmed(IDialogContext context, IAwaitable<bool> result)
        {
            bool confirm = await result;
            context.Wait(MessageReceivedAsync);
            if (confirm==true)
            {
                await new CreateLMSTicket().StartAsync(context);
                //context.Call(new CreateLMSTicket(), Resume);
            }
        }
        public async Task MessageReceivedAsync(IDialogContext context, IAwaitable<IMessageActivity> result)
        {
            var message = await result;
            var ticketNumber = new Random().Next(0, 20000);
            await context.PostAsync($"Your message '{message.Text}' was registered. Once we resolve it, we will get back to you.");
            context.Done(ticketNumber);
        }
    }
}