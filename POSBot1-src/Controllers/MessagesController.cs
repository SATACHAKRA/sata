using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using Microsoft.Bot.Builder.Dialogs;
using Microsoft.Bot.Connector;
using System.Web.Http.Description;
using System.Speech;
using Microsoft.Bot.Builder.History;
using Autofac;
using System.IO;

namespace Microsoft.Bot.Sample.LuisBot
{
    [BotAuthentication]
    public class MessagesController : ApiController
    {
        public sealed class WillActivityLogger : IActivityLogger
        {
            async Task IActivityLogger.LogAsync(IActivity activity)
            {
                var x = activity as Activity;
                string text = x.Text;
                const string path = "C:\\Users\\Anish\\Desktop\\Conversation.txt";
                using (StreamWriter sw = File.AppendText(path))
                {
                    sw.WriteLine(text);
                }
            }
        }
        static MessagesController()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<WillActivityLogger>().AsImplementedInterfaces().InstancePerDependency();
            builder.Update(Conversation.Container);
        }
        /// <summary>
        /// POST: api/Messages
        /// receive a message from a user and send replies
        /// </summary>
        /// <param name="activity"></param>
        [ResponseType(typeof(void))]
        public virtual async Task<HttpResponseMessage> Post([FromBody] Activity activity)
        {
            // check if activity is of type message
            if (activity.GetActivityType() == ActivityTypes.Message)
            {
                var connector = new ConnectorClient(new Uri(activity.ServiceUrl));
                Activity isTypingReply = activity.CreateReply();
                isTypingReply.Type = ActivityTypes.Typing;
                await connector.Conversations.ReplyToActivityAsync(isTypingReply);
                await Conversation.SendAsync(activity, () => new BasicLuisDialog());
            }
            else
            {
                await HandleSystemMessageAsync(activity);
            }
            return new HttpResponseMessage(System.Net.HttpStatusCode.Accepted);
        }

        private async Task<Activity> HandleSystemMessageAsync(Activity message)
        {
            if (message.Type == ActivityTypes.DeleteUserData)
            {
                // Implement user deletion here
                // If we handle user deletion, return a real message
            }
            else if (message.Type == ActivityTypes.ConversationUpdate)
            {
                // Handle conversation state changes, like members being added and removed
                // Use Activity.MembersAdded and Activity.MembersRemoved and Activity.Action for info
                // Not available in all channels
                if (message.MembersAdded.Any(o => o.Id == message.Recipient.Id))
                {
                    ConnectorClient client = new ConnectorClient(new Uri(message.ServiceUrl));

                    var reply = message.CreateReply();

                    using (System.Speech.Synthesis.SpeechSynthesizer syth = new System.Speech.Synthesis.SpeechSynthesizer())
                    {
                        syth.SetOutputToDefaultAudioDevice();
                        syth.Speak("Hi, I am Lucy. ");
                        syth.SetOutputToDefaultAudioDevice();
                        syth.Speak("I can help you with POS or E I D issue.");
                        syth.SetOutputToDefaultAudioDevice();
                        syth.Speak("Try asking me things like 'I have some issue', 'Error in POS open', 'E I D Merge' etc");

                    }

                    reply.Text = "Hi, I am Lucy. I can help you with POS or EID Merge issue.Try asking me things like 'I have some issue', 'Error in POS open', 'EID Merge' etc";

                    await client.Conversations.ReplyToActivityAsync(reply);
                   // reply.Text = "Try asking me things like 'I have some issue', 'Error in pos open', 'edi merge' etc";
                    //await client.Conversations.ReplyToActivityAsync(reply);
                }
            }
            else if (message.Type == ActivityTypes.ContactRelationUpdate)
            {
                // Handle add/remove from contact lists
                // Activity.From + Activity.Action represent what happened
            }
            else if (message.Type == ActivityTypes.Typing)
            {
                // Handle knowing tha the user is typing
            }
            else if (message.Type == ActivityTypes.Ping)
            {
            }

            return null;
        }
    }
}